def smbus_2bytes_to_float(data):
    expo = ((data & 0xf8)>>3) 
    if expo == 1:
        expo = -7
    if expo > 2**4:
        expo = expo-2**5
    mantisse = ((data & 0x7)<<8) + ((data & 0xff00) >> 8)
    output = mantisse * 2**expo
    return output

def smbus_2bytes_to_float_exp12(data,expo=-12):
    mantisse = ((data & 0xff)<<8) + ((data & 0xff00) >> 8)
#    print(data,mantisse,expo)
    output = mantisse * 2**expo
    return output

def smbus_2bytes_to_float_exp13(data):
    return smbus_2bytes_to_float_exp12(data,expo=-13)


