from enum import Enum
from recordclass import recordclass

class InstType (Enum):
    varSet  = 0
    varRead = 1
    method  = 2

OPCUAset=recordclass("OPCUAset","id type data mask")
