RCU=[0]
name="HBA_uC_Timeout_RXTX"
NewVal=1536

from test_common import *
import numpy as np

setRCUmask(RCU)


val=get_debug_value(name+"_R")
print("old:",val)
for r in RCU:
  val[r]=NewVal

set_debug_value(name+"_RW",val)
print("set:",val)
time.sleep(0.1)
val=get_debug_value(name+"_R")
print("new:",val)

disconnect()
