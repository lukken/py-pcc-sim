from test_common import *

name="RCU_band"
RCU=0;
Att=[2,2,2]


setAntmask([RCU])

att=get_value(name+"_R")
print("Att old:",att[3*RCU:3*RCU+3])

att[3*RCU:3*RCU+3]=Att
set_value(name+"_RW",att)

time.sleep(0.5)
att=get_value(name+"_R")
print("Att new:",att[3*RCU:3*RCU+3])

disconnect()