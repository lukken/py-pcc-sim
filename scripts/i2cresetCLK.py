#from gpiozero import LED,Button
import RPi.GPIO as GPIO
SCL=24
SDA=23
Addr=0x20 #SWITCH
#Addr=0x20 #io expander
Value=0
data=[(Addr<<1)+0,Value] #Write
#data=[Addr<<1+1] #Read

GPIO.setmode(GPIO.BCM)

GPIO.setup(SDA,GPIO.IN)
GPIO.setup(SCL,GPIO.IN)
print("SDA high",GPIO.input(SDA)==1)
print("SCL high",GPIO.input(SCL)==1)

GPIO.setup(SDA,GPIO.OUT)
GPIO.setup(SCL,GPIO.OUT)

GPIO.output(SDA,1)
GPIO.output(SCL,1)

#Start
#GPIO.output(SDA,0)
#GPIO.setup(SDA,GPIO.OUT)
GPIO.output(SDA,0)
GPIO.output(SCL,0)

for b in data:
 ba="{0:{fill}8b}".format(b,fill='0')
 print("Sending",ba)
 for bit in ba:
#   if int(bit)==0: 
#      GPIO.setup(SDA,GPIO.OUT)
#      GPIO.output(SDA,0)
#   else:
#      GPIO.setup(SDA,GPIO.IN)
   GPIO.output(SDA,int(bit))
   GPIO.output(SCL,1)
   GPIO.output(SCL,0)


 GPIO.setup(SDA,GPIO.IN)
# GPIO.output(SDA,0)
# print("Ack",GPIO.input(SDA)==0)
# GPIO.setup(SDA,GPIO.OUT)

 #ack clock
 GPIO.output(SCL,1)
# GPIO.setup(SDA,GPIO.IN)
 print("Ack",GPIO.input(SDA)==0)
 GPIO.output(SCL,0)
 print("Ack released",GPIO.input(SDA)==1)
 GPIO.setup(SDA,GPIO.OUT)

#Stop
#GPIO.output(SDA,0)
#GPIO.setup(SDA,GPIO.OUT)
GPIO.output(SDA,0)
GPIO.output(SCL,1)
GPIO.output(SDA,1)
#GPIO.setup(SDA,GPIO.IN)

GPIO.setup(SDA,GPIO.IN)
GPIO.setup(SCL,GPIO.IN)
print("SDA high",GPIO.input(SDA)==1)
print("SCL high",GPIO.input(SCL)==1)
GPIO.cleanup()

