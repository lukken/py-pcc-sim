RCU=3
HBAT=1 #HBAT on RCU 0..2
#HBA=5; #HBA Element in HBAT
#BFX=11 #delay in 0.5ns
#BFY=BFX+1
name="HBA_element_pwr"

from test_common import *
import numpy as np

AntMask=[(x==HBAT) for x in range(3)]
setAntmask([RCU],AntMask)

i=(RCU*3+HBAT)*32

val=get_value(name+"_R")
print("old:",val[i:i+32])

val[i:i+32]=np.ones([32])
#val[i:i+32]=np.zeros([32])

set_value(name+"_RW",val)

#time.sleep(0.5)
#busy=get_value("RCU_state_R")
#print(busy)
for x in range(10):
  busy=get_value("RCU_state_R")
#  print(busy)
  if (busy=='busy'): break;
  time.sleep(0.05)
while not(busy=='ready'):
  busy=get_value("RCU_state_R")
#  print(busy)
  time.sleep(0.05)

val=get_value(name+"_R")
print("new:",val[i:i+32])


disconnect()
