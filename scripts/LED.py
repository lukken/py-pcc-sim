from test_common import *

name="RCU_LED0"
RCU=0;
LEDvalue=2;

setRCUmask([RCU])

led=get_value(name+"_R")
print("LED old:",led)
led[RCU]=LEDvalue

set_value(name+"_RW",led)
time.sleep(0.1)

print("LED new:",get_value(name+"_R"))

disconnect()
