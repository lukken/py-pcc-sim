RCU=0
name="HBA_element_beamformer_delays"
AntMask=[True,True,True]
NewVal1=range(32)
NewVal2=[1]*32
NewValues=[NewVal1,NewVal2,NewVal1,NewVal2,NewVal1,NewVal2,NewVal1,NewVal2,NewVal1,NewVal2]
#NewValues=[NewVal1,NewVal2]
DEBUG=False

from test_common import *
import numpy as np

setAntmask([RCU],AntMask)

i=(RCU*3)*32

val=get_value(name+"_R")
if DEBUG:
  print("Current values:");
  for x in range(3): print(val[i+x*32:i+(x+1)*32])

for cnt,NewVal in enumerate(NewValues):

 for x in range(3): val[i+x*32:i+(x+1)*32]=NewVal

 set_value(name+"_RW",val)
 if DEBUG:
   print("set:")
   for x in range(3): print(val[i+x*32:i+(x+1)*32])
 time.sleep(0.5)
 wait_not_busy("RCU_translator_busy_R",timeout_sec=2)
# time.sleep(0.1)
 val=get_value(name+"_R")
 if DEBUG:
   print("readback:")
   for x in range(3): print(val[i+x*32:i+(x+1)*32])

 same=True;
 for x in range(3): 
   for y in range(32): 
     if not(val[i+x*32+y]==NewVal[y]): same=False;
 print(cnt+1,"Success" if same else "FAILED!")
#time.sleep(5)
#val[i:i+64]=[0]*64

#set_value(name+"_RW",val)
#print("set:",val[i:i+64])
#time.sleep(1)
#val=get_value(name+"_R")
#print("new:",val[i:i+64])

disconnect()
