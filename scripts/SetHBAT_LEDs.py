RCU=0
#HBAT=1 #HBAT on RCU 0..2
#HBA=5; #HBA Element in HBAT
#BFX=11 #delay in 0.5ns
#BFY=BFX+1
name="HBA_element_led"
AntMask=[True,True,True]
NewVal=[True]*64

from test_common import *
import numpy as np

setAntmask([RCU],AntMask)

i=(RCU*3)*32

val=get_value(name+"_R")
print("old:",val[i:i+64])

val[i:i+64]=NewVal

set_value(name+"_RW",val)
print("set:",val[i:i+64])
time.sleep(2)
val=get_value(name+"_R")
print("new:",val[i:i+64])

#time.sleep(5)
#val[i:i+64]=[0]*64

#set_value(name+"_RW",val)
#print("set:",val[i:i+64])
#time.sleep(1)
#val=get_value(name+"_R")
#print("new:",val[i:i+64])

disconnect()
