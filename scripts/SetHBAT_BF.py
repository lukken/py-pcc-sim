RCU=3
HBAT=1 #HBAT on RCU 0..2
#HBA=5; #HBA Element in HBAT
#BFX=11 #delay in 0.5ns
#BFY=BFX+1
name="HBA_element_beamformer_delays"

from test_common import *
import numpy as np

AntMask=[(x==HBAT) for x in range(3)]
setAntmask([RCU],AntMask)

i=(RCU*3+HBAT)*32

val=get_value(name+"_R")
print("old:",val[i:i+32])

val[i:i+32]=np.array(range(32))[::]*0+1

set_value(name+"_RW",val)
time.sleep(1)
val=get_value(name+"_R")
print("new:",val[i:i+32])

disconnect()
