from test_common import *


RCU=3;
connect()
setAntmask([RCU])
setRCUmask([RCU])
#call_debug_method("DTH_off")

if True:
 name="RCU_DTH_freq"
 Freq=[102e6,102.101e6,102.2e6]
# Freq=[102.2e6,102.1e6,102.0e6]
# Freq=[0,0,0]
 att=get_value(name+"_R")
 print("freq old:",att[3*RCU:3*RCU+3])
 att[3*RCU:3*RCU+3]=[int(x) for x in Freq]
 print("freq set:",att[3*RCU:3*RCU+3])
 set_value(name+"_RW",att)
 time.sleep(0.5)
 att=get_value(name+"_R")
 print("freq new :",att[3*RCU:3*RCU+3])
callmethod("RCU_DTH_on")
#callmethod("DTH_off")

disconnect()