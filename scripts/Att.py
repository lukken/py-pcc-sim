from test_common import *
connect()

name="RCU_attenuator_dB"
RCU=[1];
Att=[10,10,10]
#RCU=[1,2,3];
#Att=[0,0,0]


setAntmask(RCU)

att=get_value(name+"_R")
print("Att old:",att[:12])
for r in RCU:
  att[3*r:3*r+3]=Att
print("Att set:",att[:12])
set_value(name+"_RW",att)

time.sleep(0.5)
att=get_value(name+"_R")
print("Att new:",att[:12])

disconnect()