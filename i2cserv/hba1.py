import numpy as np
from .hwdev import hwdev
import logging
from .i2c_array import ApplyMask
from time import sleep
from queuetypes import *
import signal
try:
  import RPi.GPIO as GPIO
except:
  GPIO=False;

class hba1(hwdev):
  def __init__(self,config):
    hwdev.__init__(self,config);
    logging.info("HBA1 driver loaded")
#    self.previousHBA=np.zeros([32,3,32],dtype='int')
    self.pin=config['parameters'][0];
    self.addr=config['devreg'][0]['addr']
    self.reg=config['devreg'][0]['register_R']
    if GPIO:
      GPIO.setmode(GPIO.BCM)
      GPIO.setup(self.pin,GPIO.IN)
      logging.info("HBAT wait for PPS on pin %i, TX=%i:%i",self.pin,self.addr,self.reg)
    else:
      logging.warn("RPi GPIO module not found, PPS input disable!")

  def OPCUASetVariable(self,varid,var1,data,mask):
       logging.info(str(("HBA set Var",var1['name'],data[:32*3],mask[:12])))
       self.conf['parentcls'].SetGetVarValueMask(var1,data,mask,getalso=False)
       #Wait for PPS if required else wait a bit
       if var1.get('wait')=="PPS":
         channel=GPIO.wait_for_edge(self.pin,GPIO.RISING,timeout=1500)
         self.conf['parentcls'].i2csetget(self.addr,[self.reg])
         if channel is None:
           logging.info("PPS not received!");
         sleep(0.6)
#         return False;
       elif var1.get('wait'):
         logging.debug("Wait %i ms",var1.get('wait')) 
         sleep(var1['wait']/1000.)
       data,mask2=self.conf['parentcls'].GetVarValueMask(var1,mask)
       Data=OPCUAset(varid,InstType.varSet,data.copy(),mask2.copy())
       return [Data]

  def i2csetget(self,addr,data,reg=None,read=0):
    if read==0: return self.sethba(addr,reg,data)
    elif read==1: return self.gethba(addr,reg,data)
    else: logging.warn("Not implemented!")
    return False;

  def sethba(self,addr,reg,data):
      logging.debug("SetHba addr=0x%x reg=0x%x",addr,reg)
      I2Ccallback=self.conf['parentcls'].i2csetget
      I2Ccallback(0x40,[0],read=1)#wakeup, do nothing
      sleep(0.01) 
      I2Ccallback(addr,data[:16],reg=reg)
      if len(data)>16:
        sleep(0.01) 
        I2Ccallback(addr,data[16:],reg=reg+16)
      sleep(0.01) 
      return True

  def gethba(self,addr,reg,data):
      I2Ccallback=self.conf['parentcls'].i2csetget
#      logging.debug("getHba addr=0x%x reg=0x%x",addr,reg)
      if not(I2Ccallback(addr,data,read=1)): return False;
#      I2Ccallback(addr,data,read=1);
      logging.debug("getHba addr=0x%x reg=0x%x data=%s",addr,reg,str((data)))
      if data is None:  return False
#      data[:]=[255]*len(data)
      return True;