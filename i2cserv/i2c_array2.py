import logging
from .i2c_array import i2c_array;
from .hwdev import hwdev
from .i2c_dev import i2c_dev


class i2c_array2(i2c_array):
    def __init__(self,config):
        i2c_dev.__init__(self,config);

#       Make array of switch states
        pars=config['parameters'];
        self.Nswitch=(len(pars)+1)//2;
        sw1=list(range(pars[0],pars[1]+1));
        sw2=([] if len(pars)<4 else list(range(pars[2],pars[3]+1)))
        sw3=([] if len(pars)<6 else list(range(pars[4],pars[5]+1)))
        sw4=([] if len(pars)<8 else list(range(pars[6],pars[7]+1)))
        sw3=sw3+sw4
        if self.Nswitch>3: self.Nswitch=3;
        self.RCU_Switch1=range(pars[0],pars[1]+1);
        self.N=len(sw1)
        if len(sw2)>0: self.N*=len(sw2)
        if len(sw3)>0: self.N*=len(sw3)
        self.I2Cmask=[0]*self.N
        self.disableI2ConError=False;
        self.sw1=[x for x in sw1 for i in range(self.N//len(sw1))]
        self.sw2=[x for x in sw2 for i in range(self.N//len(sw1)//len(sw2))]*len(sw1)
        self.sw3=[x for x in sw3]*len(sw1)*len(sw2)
        logging.debug(str(("Init",config['name'],' len=',self.N,'Nswitch=',self.Nswitch,self.sw1,self.sw2,self.sw3)))
        self.I2Ccut=3;

    def SetSwitch(self,RCUi):
        #print("Set switch element",RCUi,'=',self.sw1[RCUi],self.sw2[RCUi],(self.sw3[RCUi] if len(self.sw3)<0 else 'x'))
        self.conf['parentcls'].SetSW1(self.sw1[RCUi]);
        self.conf['parentcls'].SetSW2(self.sw2[RCUi]);
        if len(self.sw3)>0:
                self.conf['parentcls'].SetSW3(self.sw3[RCUi]);

    def SetSwitchMask(self,mask):
        sw1=[];sw2=[];sw3=[];
        m=0;
        for RCUi in range(self.N):
           if mask[RCUi]: 
                 if self.sw1[RCUi] not in sw1: sw1.append(self.sw1[RCUi])
                 if self.sw2[RCUi] not in sw2: sw2.append(self.sw2[RCUi])
                 if len(self.sw3)>0:
                    if self.sw3[RCUi] not in sw3: sw3.append(self.sw3[RCUi])
        logging.info(str(("SetSwitchMask",sw1,sw2,sw3)))
        self.conf['parentcls'].SetSWx(sw1,sw2,sw3)
#        self.conf['parentcls'].SetSW1(self.sw1[RCUi]);
        
# m|=1<<self.RCU_Switch1[RCUi];
#        self.conf['parentcls'].SetChannel(m);

