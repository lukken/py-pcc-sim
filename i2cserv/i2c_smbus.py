import os
#if os.sys.platform is 'linux':
#import pylibi2c;
import smbus
import time
import logging
#read=0: write to register
#read=1: read from register
#read=2: write to register (common in group)
#read=3: wait ms second
from .hwdev import hwdev;

class i2c_smbus(hwdev):
    def __init__(self,config):
       hwdev.__init__(self,config);
#       self.I2Cdev='/dev/i2c-'+str(config['parameters'][0]);
       self.bus_nr=config['parameters'][0]
       logging.info("smbus driver on bus "+str(self.bus_nr))
       self.bus = smbus.SMBus(self.bus_nr)
       self.I2Ccounter=0

    def i2csetget(self,addr,data,reg=None,read=0):
       try:
#       if True:
              if read==3:
                     time.sleep(data[0]/1000.)
                     return True
#              bus=pylibi2c.I2CDevice(self.I2Cdev,addr)
              length=len(data)
              if read==1:
                     if not(reg is None):
                          data[:]=self.bus.read_i2c_block_data(addr, reg, length)
                     elif length==1:
                          data[0]=self.bus.read_byte(addr) 
                     elif length==2:
                          d=self.bus.read_word(addr)
                          data[0]=(d>>8)&255;
                          data[1]=d&255; 
                     else:
                       for i in range(length):
                          data[i]=self.bus.read_byte(addr) #Not vey efficient!!
                     logging.debug(str(("I2C get",addr,reg,data,read)))
              else:
                     if not(reg is None): 
                            self.bus.write_i2c_block_data(addr, reg, data)
                     elif length==1:
                          self.bus.write_byte(addr,data[0]) 
                     elif length==2:
                          self.bus.write_word(addr,data[0]<<8+data[1]) 
                     else:
                       for i in range(length):
                          self.bus.write_byte(addr,data[i]) #Not vey efficient!!
                     logging.debug(str(("I2C set",addr,reg,bytes(bytearray(data)),read)))
              return True;
       except:
#       else:
              logging.debug("I2C failed!")
#              data[:]=0
              return False;

