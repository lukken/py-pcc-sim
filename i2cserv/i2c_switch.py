import logging
from .i2c import i2c

#import HWconf
#SWaddr=0x70
class i2c_switch(i2c):
    def __init__(self,config):
        i2c.__init__(self,config)
        self.SWaddr=config['devreg'][0]['addr']
        self.CurrentChannel=0
        self.NoSwitch=self.SWaddr==0;
        logging.info("i2c switch at address "+str(self.SWaddr))
        if self.NoSwitch:
           logging.warn("i2c switch disabled!")

    def SetSW1(self,channelbit):
        channel=(0 if (channelbit>5) else 1<<(channelbit)) #LTS
        if (channel)==self.CurrentChannel: return True;
        logging.debug("SetChannelbit=%i" % channelbit)
        self.CurrentChannel=channel
        return (True if self.NoSwitch else self.i2csetget(self.SWaddr,[channel]))

    def SetChannel(self,channel):
        channel&=0x3F;#LTS
        if (channel)==self.CurrentChannel: return True;
        logging.debug("SetChannel=%i" % channel)
        self.CurrentChannel=channel
        return (True if self.NoSwitch else self.i2csetget(self.SWaddr,[channel]))

#    def I2Ccallback(self,RCU,addr,data,reg=None,read=0):
#        self.callback1(addr,data,reg,read)    
