import numpy as np
import logging
from .spibitbang1 import spibitbang1
from queuetypes import *
from .hwdev import hwdev
from .i2c_dev import *


class i2c_array(i2c_dev):
    def __init__(self,config):
        i2c_dev.__init__(self,config);
#        self.Qout=Qout;
#        self.Qin=Qin;
#        self.I2Ccallback=I2Ccallback
#        self.SWcallback=Switchcallback
#        self.previousHBA=np.zeros([number,3,32],dtype='int')
        pars=config['parameters'];
        self.RCU_Switch1=range(pars[0],pars[1]+1);
        self.N=len(self.RCU_Switch1);
        self.I2Cmask=[0]*self.N
        self.I2Ccut=pars[2];
        self.disableI2ConError=pars[2]>0;
#        self.devregs,RCU_storeReg=DevRegList(yaml)
#        print("Init",config['name'],'len=',len(self.RCU_Switch1),' stored reg=',RCU_storeReg)
#        self.previous   =np.zeros([self.N,RCU_storeReg],dtype='int')

    def OPCUAReadVariable(self,varid,var1,mask):
      if len(mask)==0: mask=[True]*self.N; 
      return i2c_dev.OPCUAReadVariable(self,varid,var1,mask)

    def SetSwitch(self,RCUi):
        self.conf['parentcls'].SetSW1(self.RCU_Switch1[RCUi]);

    def SetSwitchMask(self,mask):
        m=0;
        for RCUi in range(self.N):
           if (mask[RCUi]) and (self.I2Cmask[RCUi]<=self.I2Ccut): 
               m|=1<<self.RCU_Switch1[RCUi];
        self.conf['parentcls'].SetChannel(m);

    def SetGetVarValueMask(self,var1,data,mask,getalso=True):
        Step,Step2=GetSteps(var1);
        value1=[0]*Step*Step2;
        if len(data)==Step:
           data=data*self.N;
        if not(len(data)==Step*Step2):
            print("Check data length!");
            return;
        Step2//=self.N
        if (len(mask)==self.N):
          mask=[m for m in mask for x in range(Step)]
        if not(len(mask)==Step*self.N):
            print("Check mask length!",len(mask),Step,self.N);
            return;
#        if (len(value1)==V1.nVars) and (self.N>1):  value1=(value1*self.N);
#        logging.debug(str(("Step=",Step,"Mask=",mask)))
        for RCUi in range(self.N):
            for Vari in range(Step):
                if not(mask[RCUi*Step+Vari]): continue
                if not(self.I2Cmask[RCUi]<=self.I2Ccut):
                  mask[RCUi*Step+Vari]=False;
                  continue;
                i0=(RCUi*Step+    Vari)*Step2
                i1=(RCUi*Step+(Vari+1))*Step2
                devreg=var1['devreg'][Vari];
                width=var1.get('width',8)
                bitoffset=GetField(var1,'bitoffset',Vari,0)
                self.SetSwitch(RCUi);
                self.RCUi=RCUi;
                res=self.SetVarValue(devreg,width,bitoffset,data[i0:i1])
                if not(res):
                  if self.disableI2ConError: self.I2Cmask[RCUi]+=1;
                  if self.I2Cmask[RCUi]>self.I2Ccut: mask[RCUi*Step+Vari]=False;
                  continue;
                self.I2Cmask[RCUi]=0;
                if getalso:
                  value2=value1[i0:i1]
                  res=self.GetVarValue(devreg,width,bitoffset,value2)
                  if not(res):
                    if self.disableI2ConError: self.I2Cmask[RCUi]+=1;
                    if self.I2Cmask[RCUi]>self.I2Ccut: mask[RCUi*Step+Vari]=False;
                    continue;
                  value1[i0:i1]=value2
                  self.I2Cmask[RCUi]=0;
        return value1,mask


    def GetVarValueMask(self,var1,mask):
        Step,Step2=GetSteps(var1);
        value1=[0]*Step*Step2;
        Step2//=self.N
        if (len(mask)==self.N):
          mask=[m for m in mask for x in range(Step)]
        if not(len(mask)==Step*self.N):
            print("Check mask length!",len(mask),Step,self.N);
            return;
#        if (len(value1)==V1.nVars) and (self.N>1):  value1=(value1*self.N);
        i2c=self.conf['parentcls'];
#        logging.debug(str(("Step=",Step,"Mask=",mask)))
        mode=(1 if var1.get('read_parallel') else 0)
        for Vari in range(Step):
          if mode==1:
#            logging.debug("parallel write register for read")
#            print(mask2);
               devreg=var1['devreg'][Vari];
               width=var1.get('width',8)
               bitoffset=GetField(var1,'bitoffset',Vari,0)
               mask2=[mask[RCUi*Step+Vari] & (self.I2Cmask[RCUi]<=self.I2Ccut) for RCUi in range(self.N)]
               self.SetSwitchMask(mask2);
               res=self.GetVarValue(devreg,width,bitoffset,[],mode=3) #wait if needed
               res=self.GetVarValue(devreg,width,bitoffset,[],mode=2) #only set register
               res=self.GetVarValue(devreg,width,bitoffset,[],mode=3) #wait if needed
#            for Vari in range(Step):
#        else:
          for RCUi in range(self.N):
#            for Vari in range(Step):
                if not(mask[RCUi*Step+Vari]): continue
                i0=(RCUi*Step+    Vari)*Step2
                i1=(RCUi*Step+(Vari+1))*Step2
                if not(self.I2Cmask[RCUi]<=self.I2Ccut):
#                  mask[RCUi*Step+Vari]=False;  #Use this if we do not what to update opc-ua var with NaN
                  value1[i0]=None
                  continue;
                devreg=var1['devreg'][Vari];
                width=var1.get('width',8)
                bitoffset=GetField(var1,'bitoffset',Vari,0)
                self.SetSwitch(RCUi);
                value2=value1[i0:i1]
                self.RCUi=RCUi;
                res=self.GetVarValue(devreg,width,bitoffset,value2,mode=mode)
                if not(res):
                  if self.disableI2ConError: self.I2Cmask[RCUi]+=1;
                  if self.I2Cmask[RCUi]>self.I2Ccut: mask[RCUi*Step+Vari]=False;
                  value1[i0]=None
                  continue;
                self.I2Cmask[RCUi]=0;
                value1[i0:i1]=value2
        return value1,mask

    def getstorearray(self,devreg,N=1):
          storearray=devreg.get('storearray')
          if not(storearray):
                devreg['storearray']=([0] if N==1 else [[0]*N])*self.N;
                storearray=devreg.get('storearray');
          return storearray;

    def Setdevreg(self,devreg,value,mask=[]):
#        if devreg.get('store'): logging.debug("Stored")
#        print(devreg['store'])
        if len(mask)==0: 
             mask=[True]*self.N;
             self.RCUi=0; 
        self.SetSwitchMask(mask)
        if not(self.SetVarValue(devreg,8,0,value)): return False;
        if devreg.get('store'):
                storearray=self.getstorearray(devreg);
                for RCUi in range(self.N):
                  if (mask[RCUi]) and (self.I2Cmask[RCUi]<=self.I2Ccut) and not(value[0] is None):   
                      storearray[RCUi]=value[0]
                      self.RCUi=RCUi;
                logging.debug(str(("Stored values:",self.getstorearray(devreg))))
        return True;
        

    def Getdevreg(self,devreg,mask=[]):
        value1=[0]*self.N
        mask=[True]*self.N
        if devreg.get('store'):
                storearray=self.getstorearray(devreg);
        i2c=self.conf['parentcls'];
        for RCUi in range(self.N):
                if not(mask[RCUi]): continue
                if not(self.I2Cmask[RCUi]<=self.I2Ccut): continue
                self.SetSwitch(RCUi);
                value2=[value1[RCUi]]
                self.RCUi=RCUi;
                res=self.GetVarValue(devreg,8,0,value2)
                if not(res):
                    if self.disableI2ConError: self.I2Cmask[RCUi]+=1;
                self.I2Cmask[RCUi]=0; 
                value1[RCUi]=value2[0]
                if devreg.get('store'):
                  if mask[RCUi] and not(value2[0] is None):   
                      storearray[RCUi]=value2[0]
        logging.debug(str(("Stored values:",self.getstorearray(devreg))))
        return True;

    def SetVarValue(self,devreg,width,bitoffset,value):
            if devreg['register_W']==-1: return True; #We can not set it, only read it e.g. temperature
            logging.debug(str(("RCU1 Set ",self.RCUi,devreg['addr'],value)))
            #self.parentcls.SetChannel(1<<RCU_Switch[RCUi]);
            if devreg['store']:
                storearray=self.getstorearray(devreg);
                previous=storearray[self.RCUi];
#                print(value,previous)
                for x in range(len(value)):
                  value[x]=ApplyMask(value[x],width,bitoffset,(previous[x] if isinstance(previous,list) else previous));
                storearray[self.RCUi]=(value[0] if len(value)==1 else value[:])
                logging.debug("Stored value:"+str(storearray[self.RCUi]))
            #  devreg['drivercls'].i2csetget
            return devreg['drivercls'].i2csetget(devreg['addr'],value,reg=devreg['register_W'])

    def GetVarValue(self,devreg,width,bitoffset,value,mode=0):
        logging.debug(str(("RCU1 Get ",self.RCUi,devreg['addr'],value)))
#                self.GetI2C(RCUi,devreg,width,bitoffset,value)
#        if dev.store>0:
#            value[0]=self.previous[RCUi,dev.store-1]
#        return True
        callback=devreg['drivercls'].i2csetget;
        reg=devreg['register_R']

        if mode==2:
           return callback(devreg['addr'],int2bytes(reg),read=2)
        elif mode==3:
           if devreg.get('wait',0)>0: callback(0,[devreg['wait']],read=3)
           elif reg>255: callback(0,[250],read=3)
           return True;

        l1=int(np.floor((width+bitoffset+7)/8))
#        print(width,bitoffset,l1)
        value2=value
        value2[0]=None; #default
        if mode==1:
           if not(callback(devreg['addr'],value2,read=1)): return False;
        else:
          if reg>255: #This is for the monitor ADC (+ DTH)
            callback(0,[250],read=3)
            if not(callback(devreg['addr'],int2bytes(reg),read=2)): return False;
            callback(0,[250],read=3)
            if not(callback(devreg['addr'],value2,read=1)): return False;
          else:
            if not(callback(devreg['addr'],value2,reg=reg,read=1)): return False;
        if value2[0] is None:  return False
        value[:]=value2[:];
        if devreg.get('store'):
             storearray=self.getstorearray(devreg,len(value));
             storearray[self.RCUi]=(value[0] if len(value)==1 else value[:])
             logging.debug(str(("Store buffer",self.RCUi,storearray[self.RCUi])))
 #            print("Stored values:",self.getstorearray(devreg))
        if (width!=l1*8) or (bitoffset>0):
            if (width<8):
              for i in range(len(value)):
                value[i]=UnMask(value[i],width,bitoffset)
            else:
                value[0]=UnMask(value[0],width-(l1-1)*8,bitoffset)
        else: value[0]=value2[0]
        return True;


