import logging
import argparse
from opcuaserv import opcuaserv
from opcuaserv import i2client
from opcuaserv import yamlreader
#from opcuaserv import pypcc2
from i2cserv import i2cthread
import threading
import time
import sys
import signal

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--simulator", help="Do not connect to I2c, but simulate behaviour.", action="store_true")
parser.add_argument("-t", "--test", help="Do not start OPC-UA server.", action="store_true")
parser.add_argument("-p", "--port", help="Port number to listen on [%(default)s].", type=int, default=4842)
parser.add_argument("-l", "--loglevel", help="Log level [%(default)s].", type=str, choices=["DEBUG","INFO","WARNING","ERROR"], default="INFO")
parser.add_argument("--loghost", help="Logstash host to which to forward logs [%(default)s]",type=str, default='')
parser.add_argument("-c", "--config", help="YAML config files, comma seperated [%(default)s]",type=str, default='RCU')
args = parser.parse_args()

from logconfig import configure_logger
log_extra = {
    "simulator": args.simulator, 
    "test": args.test,
    "port": args.port,
    "config": args.config,
    "lofar_id": f"pypcc - {args.config}",
}
configure_logger(logstash_host=args.loghost,level=args.loglevel, log_extra=log_extra)

RunTimer=True;
def signal_handler(sig, frame):
    logging.warn('Stop signal received!')
    global RunTimer; 
    RunTimer=False
signal.signal(signal.SIGINT, signal_handler)

#Start i2c processes as soon as possible to have minimum duplication
logging.info("Start I2C processes")   
#I2Cports=['UNB2','RCU','CLK']
#I2Cports=['RCU']
I2Cports=[x for x in args.config.split(',')]
threads=[]
I2Cclients=[]
for name in I2Cports:
    RCU_I2C=i2client.i2client(name=name)
    if not(args.simulator):  
        thread1=i2cthread.start(*RCU_I2C.GetInfo())
        threads.append(thread1)
    I2Cclients.append(RCU_I2C)

#Initialise OPCUA server and load variables
logging.info("Initialised OPC-UA Server")   
configs=[]
if not(args.test):  
    opcuaserv.InitServer(port=args.port)
    logging.info("Load OPCUA variables & start i2c listing thread")   
    for i,name in enumerate(I2Cports):
        RCU_I2C=I2Cclients[i]
        RCU_conf=yamlreader.yamlreader(RCU_I2C,yamlfile=name)
        RCU_conf.AddVars(opcuaserv.AddVarR,opcuaserv.AddVarW)
        RCU_conf.AddMethod(opcuaserv.Addmethod)
        RCU_conf.CallInit();
        configs.append(RCU_conf);

        thread2=threading.Thread(target=RCU_conf.getvar); #Thread on OPC-UA side of pipe
        thread2.start()
        threads.append(thread2)
    time.sleep(1)
    logging.info("Start OPC-UA server")
    opcuaserv.start()
logging.getLogger().setLevel("WARNING")

if False:
   opcuaserv.server.stop()
   exit()

 
try:
 while RunTimer:
    time.sleep(0.1);
    for c in configs:
        c.Monitor();
finally:
   if not(args.test):
       logging.info("Stop OPC-UA server")
       opcuaserv.server.stop()

logging.info("Stop threads")
for i2c in I2Cclients:
    i2c.stop()
for thread1 in threads:
    thread1.join()
